<h1 align="center">Operating System 👋</h1>
<p>
  <a href="https://gitlab.com/randop/os">
    <img src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  </a>
  <a href="https://gitlab.com/randop/os">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" target="_blank" />
  </a>
  <a href="https://gitlab.com/randop/os/-/commits/main">
    <img alt="Maintenance" src="https://img.shields.io/badge/Maintained%3F-yes-green.svg" target="_blank" />
  </a>
</p>

## 🏠 [Homepage](https://gitlab.com/randop/os)
### The repository of my operating system assets.

## Author

👤 **Randolph Ledesma**

* 📱 +1 (415) 754-3092
* 🌐 [https://gitlab.com/randop](https://gitlab.com/randop)
* 👷 [https://www.linkedin.com/in/randop/](https://www.linkedin.com/in/randop/)
* 📍 Philippines

---
## 📝 License

Copyright © 2010 — 2023 [Randolph Ledesma](https://gitlab.com/randop).

Last updated on 2023-09-24T06:43:40.000Z
